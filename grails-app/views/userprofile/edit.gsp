

<%@ page import="com.invbox.bo.Userprofile" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'userprofile.label', default: 'Userprofile')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${userprofileInstance}">
            <div class="errors">
                <g:renderErrors bean="${userprofileInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${userprofileInstance?.id}" />
                <g:hiddenField name="version" value="${userprofileInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="profileCode"><g:message code="userprofile.profileCode.label" default="Profile Code" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'profileCode', 'errors')}">
                                    <g:textField name="profileCode" value="${userprofileInstance?.profileCode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="profileName"><g:message code="userprofile.profileName.label" default="Profile Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'profileName', 'errors')}">
                                    <g:textField name="profileName" value="${userprofileInstance?.profileName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="firstname"><g:message code="userprofile.firstname.label" default="Firstname" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'firstname', 'errors')}">
                                    <g:textField name="firstname" value="${userprofileInstance?.firstname}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="lastname"><g:message code="userprofile.lastname.label" default="Lastname" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'lastname', 'errors')}">
                                    <g:textField name="lastname" value="${userprofileInstance?.lastname}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="email"><g:message code="userprofile.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${userprofileInstance?.email}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="address"><g:message code="userprofile.address.label" default="Address" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'address', 'errors')}">
                                    <g:textArea name="address" cols="40" rows="5" value="${userprofileInstance?.address}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="retainHistory"><g:message code="userprofile.retainHistory.label" default="Retain History" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'retainHistory', 'errors')}">
                                    <g:textField name="retainHistory" value="${fieldValue(bean: userprofileInstance, field: 'retainHistory')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="password"><g:message code="userprofile.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value="${userprofileInstance?.password}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="dev_appkey"><g:message code="userprofile.dev_appkey.label" default="Devappkey" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'dev_appkey', 'errors')}">
                                    <g:textField name="dev_appkey" value="${userprofileInstance?.dev_appkey}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="histories"><g:message code="userprofile.histories.label" default="Histories" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'histories', 'errors')}">
                                    
<ul>
<g:each in="${userprofileInstance?.histories?}" var="h">
    <li><g:link controller="history" action="show" id="${h.id}">${h?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="history" action="create" params="['userprofile.id': userprofileInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'history.label', default: 'History')])}</g:link>

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
