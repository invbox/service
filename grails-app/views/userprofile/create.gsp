

<%@ page import="com.invbox.bo.Userprofile" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'userprofile.label', default: 'Userprofile')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${userprofileInstance}">
            <div class="errors">
                <g:renderErrors bean="${userprofileInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="profileCode"><g:message code="userprofile.profileCode.label" default="Profile Code" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'profileCode', 'errors')}">
                                    <g:textField name="profileCode" value="${userprofileInstance?.profileCode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="profileName"><g:message code="userprofile.profileName.label" default="Profile Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'profileName', 'errors')}">
                                    <g:textField name="profileName" value="${userprofileInstance?.profileName}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="firstname"><g:message code="userprofile.firstname.label" default="Firstname" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'firstname', 'errors')}">
                                    <g:textField name="firstname" value="${userprofileInstance?.firstname}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastname"><g:message code="userprofile.lastname.label" default="Lastname" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'lastname', 'errors')}">
                                    <g:textField name="lastname" value="${userprofileInstance?.lastname}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email"><g:message code="userprofile.email.label" default="Email" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'email', 'errors')}">
                                    <g:textField name="email" value="${userprofileInstance?.email}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="address"><g:message code="userprofile.address.label" default="Address" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'address', 'errors')}">
                                    <g:textArea name="address" cols="40" rows="5" value="${userprofileInstance?.address}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="retainHistory"><g:message code="userprofile.retainHistory.label" default="Retain History" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'retainHistory', 'errors')}">
                                    <g:textField name="retainHistory" value="${fieldValue(bean: userprofileInstance, field: 'retainHistory')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password"><g:message code="userprofile.password.label" default="Password" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'password', 'errors')}">
                                    <g:textField name="password" value="${userprofileInstance?.password}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dev_appkey"><g:message code="userprofile.dev_appkey.label" default="Devappkey" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: userprofileInstance, field: 'dev_appkey', 'errors')}">
                                    <g:textField name="dev_appkey" value="${userprofileInstance?.dev_appkey}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
