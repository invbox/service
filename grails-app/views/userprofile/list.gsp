
<%@ page import="com.invbox.bo.Userprofile" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'userprofile.label', default: 'Userprofile')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'userprofile.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="profileCode" title="${message(code: 'userprofile.profileCode.label', default: 'Profile Code')}" />
                        
                            <g:sortableColumn property="profileName" title="${message(code: 'userprofile.profileName.label', default: 'Profile Name')}" />
                        
                            <g:sortableColumn property="firstname" title="${message(code: 'userprofile.firstname.label', default: 'Firstname')}" />
                        
                            <g:sortableColumn property="lastname" title="${message(code: 'userprofile.lastname.label', default: 'Lastname')}" />
                        
                            <g:sortableColumn property="email" title="${message(code: 'userprofile.email.label', default: 'Email')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userprofileInstanceList}" status="i" var="userprofileInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${userprofileInstance.id}">${fieldValue(bean: userprofileInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: userprofileInstance, field: "profileCode")}</td>
                        
                            <td>${fieldValue(bean: userprofileInstance, field: "profileName")}</td>
                        
                            <td>${fieldValue(bean: userprofileInstance, field: "firstname")}</td>
                        
                            <td>${fieldValue(bean: userprofileInstance, field: "lastname")}</td>
                        
                            <td>${fieldValue(bean: userprofileInstance, field: "email")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${userprofileInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
