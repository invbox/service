
<%@ page import="com.invbox.bo.Category" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'category.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="categoryname" title="${message(code: 'category.categoryname.label', default: 'Categoryname')}" />
                        
                            <g:sortableColumn property="categorydescription" title="${message(code: 'category.categorydescription.label', default: 'Categorydescription')}" />
                        
                            <g:sortableColumn property="createdby" title="${message(code: 'category.createdby.label', default: 'Createdby')}" />
                        
                            <g:sortableColumn property="creationdate" title="${message(code: 'category.creationdate.label', default: 'Creationdate')}" />
                        
                            <g:sortableColumn property="lastmodifiedby" title="${message(code: 'category.lastmodifiedby.label', default: 'Lastmodifiedby')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${categoryInstance.id}">${fieldValue(bean: categoryInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: categoryInstance, field: "categoryname")}</td>
                        
                            <td>${fieldValue(bean: categoryInstance, field: "categorydescription")}</td>
                        
                            <td>${fieldValue(bean: categoryInstance, field: "createdby")}</td>
                        
                            <td><g:formatDate date="${categoryInstance.creationdate}" /></td>
                        
                            <td>${fieldValue(bean: categoryInstance, field: "lastmodifiedby")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${categoryInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
