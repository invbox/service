
<%@ page import="com.invbox.bo.Item" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.itemcode.label" default="Itemcode" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "itemcode")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.itemname.label" default="Itemname" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "itemname")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.itemdescription.label" default="Itemdescription" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "itemdescription")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.purchaseprice.label" default="Purchaseprice" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "purchaseprice")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.purchasedate.label" default="Purchasedate" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${itemInstance?.purchasedate}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.image.label" default="Image" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "image")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.type.label" default="Type" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "type")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.status.label" default="Status" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "status")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.createdby.label" default="Createdby" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "createdby")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.creationdate.label" default="Creationdate" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${itemInstance?.creationdate}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.lastmodifiedby.label" default="Lastmodifiedby" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "lastmodifiedby")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.lastmodifieddate.label" default="Lastmodifieddate" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${itemInstance?.lastmodifieddate}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.profilecode.label" default="Profilecode" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: itemInstance, field: "profilecode")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="item.category.label" default="Category" /></td>
                            
                            <td valign="top" class="value"><g:link controller="category" action="show" id="${itemInstance?.category?.id}">${itemInstance?.category?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${itemInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
