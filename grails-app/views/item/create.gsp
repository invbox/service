

<%@ page import="com.invbox.bo.Item" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${itemInstance}">
            <div class="errors">
                <g:renderErrors bean="${itemInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="itemcode"><g:message code="item.itemcode.label" default="Itemcode" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'itemcode', 'errors')}">
                                    <g:textField name="itemcode" value="${fieldValue(bean: itemInstance, field: 'itemcode')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="itemname"><g:message code="item.itemname.label" default="Itemname" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'itemname', 'errors')}">
                                    <g:textArea name="itemname" cols="40" rows="5" value="${itemInstance?.itemname}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="itemdescription"><g:message code="item.itemdescription.label" default="Itemdescription" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'itemdescription', 'errors')}">
                                    <g:textArea name="itemdescription" cols="40" rows="5" value="${itemInstance?.itemdescription}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="purchaseprice"><g:message code="item.purchaseprice.label" default="Purchaseprice" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'purchaseprice', 'errors')}">
                                    <g:textField name="purchaseprice" value="${fieldValue(bean: itemInstance, field: 'purchaseprice')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="purchasedate"><g:message code="item.purchasedate.label" default="Purchasedate" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'purchasedate', 'errors')}">
                                    <g:datePicker name="purchasedate" precision="day" value="${itemInstance?.purchasedate}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="image"><g:message code="item.image.label" default="Image" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'image', 'errors')}">
                                    <g:textField name="image" value="${itemInstance?.image}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="item.type.label" default="Type" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'type', 'errors')}">
                                    <g:textField name="type" value="${itemInstance?.type}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="status"><g:message code="item.status.label" default="Status" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'status', 'errors')}">
                                    <g:textField name="status" value="${itemInstance?.status}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby"><g:message code="item.createdby.label" default="Createdby" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'createdby', 'errors')}">
                                    <g:textField name="createdby" value="${itemInstance?.createdby}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="creationdate"><g:message code="item.creationdate.label" default="Creationdate" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'creationdate', 'errors')}">
                                    <g:datePicker name="creationdate" precision="day" value="${itemInstance?.creationdate}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastmodifiedby"><g:message code="item.lastmodifiedby.label" default="Lastmodifiedby" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'lastmodifiedby', 'errors')}">
                                    <g:textField name="lastmodifiedby" value="${itemInstance?.lastmodifiedby}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastmodifieddate"><g:message code="item.lastmodifieddate.label" default="Lastmodifieddate" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'lastmodifieddate', 'errors')}">
                                    <g:datePicker name="lastmodifieddate" precision="day" value="${itemInstance?.lastmodifieddate}" default="none" noSelection="['': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="profilecode"><g:message code="item.profilecode.label" default="Profilecode" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'profilecode', 'errors')}">
                                    <g:textField name="profilecode" value="${itemInstance?.profilecode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="category"><g:message code="item.category.label" default="Category" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: itemInstance, field: 'category', 'errors')}">
                                    <g:select name="category.id" from="${com.invbox.bo.Category.list()}" optionKey="id" value="${itemInstance?.category?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
