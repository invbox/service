
<%@ page import="com.invbox.bo.History" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'history.label', default: 'History')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'history.id.label', default: 'Id')}" />
                        
                            <g:sortableColumn property="itemcode" title="${message(code: 'history.itemcode.label', default: 'Itemcode')}" />
                        
                            <g:sortableColumn property="listprice" title="${message(code: 'history.listprice.label', default: 'Listprice')}" />
                        
                            <g:sortableColumn property="marketplacecode" title="${message(code: 'history.marketplacecode.label', default: 'Marketplacecode')}" />
                        
                            <g:sortableColumn property="searchdate" title="${message(code: 'history.searchdate.label', default: 'Searchdate')}" />
                        
                            <g:sortableColumn property="dateCreated" title="${message(code: 'history.dateCreated.label', default: 'Date Created')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${historyInstanceList}" status="i" var="historyInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${historyInstance.id}">${fieldValue(bean: historyInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: historyInstance, field: "itemcode")}</td>
                        
                            <td>${fieldValue(bean: historyInstance, field: "listprice")}</td>
                        
                            <td>${fieldValue(bean: historyInstance, field: "marketplacecode")}</td>
                        
                            <td><g:formatDate date="${historyInstance.searchdate}" /></td>
                        
                            <td><g:formatDate date="${historyInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${historyInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
