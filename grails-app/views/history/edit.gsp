

<%@ page import="com.invbox.bo.History" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'history.label', default: 'History')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list111"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${historyInstance}">
            <div class="errors">
                <g:renderErrors bean="${historyInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${historyInstance?.id}" />
                <g:hiddenField name="version" value="${historyInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="itemcode"><g:message code="history.itemcode.label" default="Itemcode" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: historyInstance, field: 'itemcode', 'errors')}">
                                    <g:textField name="itemcode" value="${historyInstance?.itemcode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="listprice"><g:message code="history.listprice.label" default="Listprice" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: historyInstance, field: 'listprice', 'errors')}">
                                    <g:textField name="listprice" value="${fieldValue(bean: historyInstance, field: 'listprice')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="marketplacecode"><g:message code="history.marketplacecode.label" default="Marketplacecode" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: historyInstance, field: 'marketplacecode', 'errors')}">
                                    <g:textField name="marketplacecode" value="${historyInstance?.marketplacecode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="searchdate"><g:message code="history.searchdate.label" default="Searchdate" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: historyInstance, field: 'searchdate', 'errors')}">
                                    <g:datePicker name="searchdate" precision="day" value="${historyInstance?.searchdate}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="userprofile"><g:message code="history.userprofile.label" default="Userprofile" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: historyInstance, field: 'userprofile', 'errors')}">
                                    <g:select name="userprofile.id" from="${com.invbox.bo.Userprofile.list()}" optionKey="id" value="${historyInstance?.userprofile?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
