package com.invbox.bo;

class History {

	String itemcode
	String itemdescription
	Double listprice
	String marketplacecode
	Date searchdate
//	Userprofile userprofile
	Date dateCreated
	
	

	static belongsTo = [ userprofile : Userprofile ]

//	static mapping = {
//		id column: "historycode"
//		version false
//	}

	static constraints = {
		itemcode nullable: false
		itemdescription nullable: true
		listprice nullable: true
		marketplacecode nullable: true
		searchdate nullable: true
	}
}
