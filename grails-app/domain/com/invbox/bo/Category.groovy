package com.invbox.bo

class Category {
	
	String categoryname; 
	String categorydescription; 
	String createdby; 
	Date creationdate; 
	String lastmodifiedby; 
	Date lastmodifieddate;
	
	static mapping = {
		id generator: 'identity', column: "categorycode", type : 'long'
	}

    static constraints = {
		categoryname nullable: false
		categorydescription nullable: true
		createdby nullable: true
		creationdate nullable: true
		lastmodifiedby nullable: true
		lastmodifieddate nullable: true
    }
}
