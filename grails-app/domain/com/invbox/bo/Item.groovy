package com.invbox.bo

class Item {

	String itemcode
	String itemname
	String profilecode
	String itemdescription
	Double purchaseprice
	Date purchasedate
	String image
//	Integer categorycode
	String type
	String status
	String createdby
	Date creationdate
	String lastmodifiedby
	Date lastmodifieddate
	
	static mapping = {
		version false
	}
	
	static belongsTo = [category: Category]

	static constraints = {
		itemcode nullable: true
		itemname nullable: true, maxSize: 512
		itemdescription nullable: true, maxSize: 65535
		purchaseprice nullable: true
		purchasedate nullable: true
		image nullable: true
		type nullable: true
		status nullable: true
		createdby nullable: true
		creationdate nullable: true
		lastmodifiedby nullable: true
		lastmodifieddate nullable: true
		profilecode nullable: true
	}
}
