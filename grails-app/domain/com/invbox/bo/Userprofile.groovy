package com.invbox.bo

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

class Userprofile {

	String profileCode
	String profileName
	String firstname
	String lastname
	String email
	String address
	Integer retainHistory
	String password
	String dev_appkey

	static hasMany = [histories: History]

	//	static mapping = {
	//		id name: "profileCode", generator: "assigned"
	//		version falsef
	//	}

	static constraints = {
		profileCode nullable: false, unique: true
		profileName nullable: true
		firstname nullable: true
		lastname nullable: true
		email nullable: true
		address nullable: true, maxSize: 65535
		retainHistory nullable: true
		password nullable: false
		dev_appkey nullable: false
	}

	public boolean checkForAuthentication(String actualPwd, String passedPwd) throws Exception {

		if (actualPwd == null || actualPwd.length() == 0)
			throw new Exception("Empty string");

		String[] fields = new String(Base64.decode(actualPwd)).split("]");
		byte[] salt = Base64.decode(fields[1]);
		byte[] iv = Base64.decode(fields[2]);
		String currentData = justEncrypt(passedPwd, salt, iv);
		if(fields[0].equals(currentData)){
			return true;
		}else{
			return false;
		}

		/*try{
		 decrypt(actualPwd,passedPwd);
		 return true;
		 }catch(Exception){
		 return false;
		 }*/
	}

	private static String justEncrypt(String pwd, byte[] salt, byte[] iv)
	throws Exception {

		int iterationCount = 10000;
		int saltLength = 16; // bytes; 128 bits
		int keyLength = 128;
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		KeySpec keySpec = new PBEKeySpec(pwd.toCharArray(), salt,
				iterationCount, keyLength);
		SecretKeyFactory keyFactory = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA1");
		byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
		SecretKey key = new SecretKeySpec(keyBytes, "AES");

		IvParameterSpec ivParams = new IvParameterSpec(iv);

		byte[] encrypted = null;
		cipher.init(Cipher.ENCRYPT_MODE, key, ivParams);
		encrypted = cipher.doFinal(pwd.getBytes("UTF-8"));

		return Base64.encode(encrypted);
	}

	public byte[] decrypt(String encryptedPwd, String passedPwd) throws Exception {

		int iterationCount = 10000;
		int saltLength = 16; // bytes; 128 bits
		int keyLength = 128;

		if (encryptedPwd == null || encryptedPwd.length() == 0)
			throw new Exception("Empty string");

		String[] fields = new String(Base64.decode(encryptedPwd)).split("]");
		byte[] cipherBytes = Base64.decode(fields[0]);
		byte[] salt =  Base64.decode(fields[1]);
		byte[] iv =  Base64.decode(fields[2]);

		KeySpec keySpec = new PBEKeySpec(passedPwd.toCharArray(), salt,
				iterationCount, keyLength);
		SecretKeyFactory keyFactory = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA1");
		byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
		SecretKey key = new SecretKeySpec(keyBytes, "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec ivParams = new IvParameterSpec(iv);

		byte[] decrypted = null;
		cipher.init(Cipher.DECRYPT_MODE, key, ivParams);
		decrypted = cipher.doFinal(cipherBytes);

		return decrypted;
	}

	private static String undoEncrpytion(String pwd){
		int iterationCount = 10000;
		int saltLength = 16; // bytes; 128 bits
		int keyLength = 128;
		String pwdStr = new String(Base64.decode(pwd));
		println("pwdStr::"+pwdStr);
		String[] fields = pwdStr.split("]");
		byte[] salt = Base64.decode(fields[1]);
		byte[] iv = Base64.decode(fields[2]);
		byte[] cipherBytes = Base64.decode(fields[0]);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		KeySpec keySpec = new PBEKeySpec(pwdStr.toCharArray(), salt,
				iterationCount, keyLength);
		SecretKeyFactory keyFactory = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA1");
		byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
		SecretKey key = new SecretKeySpec(keyBytes, "AES");

		IvParameterSpec ivParams = new IvParameterSpec(iv);
		cipher.init(Cipher.DECRYPT_MODE, key, ivParams);
		byte[] plaintext = cipher.doFinal(cipherBytes);
		String plainStr = new String(plaintext , "UTF-8");

		println("plainStr::"+plainStr);

		return "";
	}

}
