package com.invbox.bo

import org.hibernate.id.GUIDGenerator;
import com.sun.net.httpserver.Base64;
import grails.converters.*;

class UserprofileController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def index = {
		redirect(action: "list", params: params)
	}

	def list = {
		//        params.max = Math.min(params.max ? params.int('max') : 10, 100)
		//        [userprofileInstanceList: Userprofile.list(params), userprofileInstanceTotal: Userprofile.count()]
		if(!params.max) params.max = 10
		def list = Userprofile.list(params)
		def count = Userprofile.count()
		if("xml".equals(params.requesttype)){
			render list as XML
		}else if("json".equals(params.requesttype)){
			render list as JSON
		}else{
			params.max = Math.min(params.max ? params.int('max') : 10, 100)
			[userprofileInstanceList: Userprofile.list(params), userprofileInstanceTotal: Userprofile.count()]
		}
	}

	def create = {
		def userprofileInstance = new Userprofile()
		userprofileInstance.properties = params
		return [userprofileInstance: userprofileInstance]
	}

	def save = {
		params.dev_appkey = UUID.randomUUID().toString();
		def userprofileInstance = new Userprofile(params)
		if (userprofileInstance.save(flush: true)) {
			//            flash.message = "${message(code: 'default.created.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), userprofileInstance.id])}"
			render userprofileInstance as XML
		}
		else {
			//			redirect(action: "show", id: userprofileInstance.id)
			//            render(view: "create", model: [userprofileInstance: userprofileInstance])
			render (userprofileInstance.errors) as XML
			//			render userprofileInstance as XML
		}
	}

	def show = {
		def userprofileInstance = Userprofile.get(params.id)
		if (!userprofileInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
			redirect(action: "list")
		}
		else {
			//            [userprofileInstance: userprofileInstance]
			render userprofileInstance as XML
		}
	}

	def verify = {
		def userprofileInstance = Userprofile.withCriteria {
			eq("profileCode",params.profileCode)
		}
		def passedPwd = new String(((String)params.password).getBytes());
		def actualPwd = userprofileInstance.password[0]
		//userprofileInstance[0].undoEncrpytion(passedPwd);
		if(userprofileInstance[0].checkForAuthentication(actualPwd,	new String(passedPwd.decodeBase64()))){
			println("user authentication passed::"+ params.password)
			render "true"
		}else{
			println("user authentication failed::"+ params.password)
			render "false"
		}
	}

	def edit = {
		def userprofileInstance = Userprofile.get(params.id)
		if (!userprofileInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
			redirect(action: "list")
		}
		else {
			return [userprofileInstance: userprofileInstance]
		}
	}

	def update = {
		def userprofileInstance = Userprofile.get(params.id)
		if (userprofileInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (userprofileInstance.version > version) {

					userprofileInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [
						message(code: 'userprofile.label', default: 'Userprofile')]
					as Object[], "Another user has updated this Userprofile while you were editing")
					render(view: "edit", model: [userprofileInstance: userprofileInstance])
					return
				}
			}
			userprofileInstance.properties = params
			if (!userprofileInstance.hasErrors() && userprofileInstance.save(flush: true)) {
				flash.message = "${message(code: 'default.updated.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), userprofileInstance.id])}"
				redirect(action: "show", id: userprofileInstance.id)
			}
			else {
				render(view: "edit", model: [userprofileInstance: userprofileInstance])
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
			redirect(action: "list")
		}
	}

	def delete = {
		def userprofileInstance = Userprofile.get(params.id)
		if (userprofileInstance) {
			try {
				userprofileInstance.delete(flush: true)
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
				redirect(action: "list")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'userprofile.label', default: 'Userprofile'), params.id])}"
			redirect(action: "list")
		}
	}

}
