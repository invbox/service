package com.invbox.bo

import grails.converters.*;

class HistoryController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def index = {
		redirect(action: "list", params: params)
	}

	def list = {
		if(!params.max) params.max = 10
		def list = History.list(params)
		def count = History.count()
		if("xml".equals(params.requesttype)){
			render list as XML
		}else if("json".equals(params.requesttype)){
			render list as JSON
		}else{
			render list as JSON
		}
	}
	def create = {
		def historyInstance = new History()
		historyInstance.properties = params
		return [historyInstance: historyInstance]
	}

	def save = {
		def historyInstance = new History(params)
		log.warn "params:: ${params}"
		if (historyInstance.save(flush: true)) {
			flash.message = "${message(code: 'default.created.message', args: [message(code: 'history.label', default: 'History'), historyInstance.id])}"
			redirect(action: "show", id: historyInstance.id)
		}
		else {
			render(view: "create", model: [historyInstance: historyInstance])
		}
	}

	def show = {
		def historyInstance = History.get(params.id)
		if (!historyInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
			redirect(action: "list111")
		}
		else {
			[historyInstance: historyInstance]
		}
	}

	def edit = {
		def historyInstance = History.get(params.id)
		if (!historyInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
			redirect(action: "list111")
		}
		else {
			return [historyInstance: historyInstance]
		}
	}

	def update = {
		def historyInstance = History.get(params.id)
		if (historyInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (historyInstance.version > version) {

					historyInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [
						message(code: 'history.label', default: 'History')]
					as Object[], "Another user has updated this History while you were editing")
					render(view: "edit", model: [historyInstance: historyInstance])
					return
				}
			}
			historyInstance.properties = params
			if (!historyInstance.hasErrors() && historyInstance.save(flush: true)) {
				flash.message = "${message(code: 'default.updated.message', args: [message(code: 'history.label', default: 'History'), historyInstance.id])}"
				redirect(action: "show", id: historyInstance.id)
			}
			else {
				render(view: "edit", model: [historyInstance: historyInstance])
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
			redirect(action: "list111")
		}
	}

	def delete = {
		def historyInstance = History.get(params.id)
		if (historyInstance) {
			try {
				historyInstance.delete(flush: true)
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
				redirect(action: "list111")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'history.label', default: 'History'), params.id])}"
			redirect(action: "list111")
		}
	}
}
