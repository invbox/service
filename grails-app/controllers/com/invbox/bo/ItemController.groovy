package com.invbox.bo

import grails.converters.*;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

class ItemController {

	/* Sample to control xml output for deep.
	 * class User {
	 String login
	 String name
	 String password
	 }
	 def converter = new XML(aUser)
	 converter.registerObjectMarshaller(User, {user, conv ->
	 conv.build {
	 login(user.login)
	 name(user.name)
	 }
	 })
	 or you could set the converter for the entire system by putting the same code in Bootstrap.groovy
	 grails.converters.XML.registerObjectMarshaller(User) { user, converter ->
	 // same implementation as above...
	 }
	 */

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", getItemsForSellForItemId: "GET"]

	def index = {
		redirect(action: "list", params: params)
	}

	//	def index = { redirect(action:filelist,params:params) }

	def list = {
		grails.converters.XML.use('deep')
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[itemInstanceList: Item.list(params), itemInstanceTotal: Item.count()]
	}

	def create = {
		def itemInstance = new Item()
		itemInstance.properties = params
		return [itemInstance: itemInstance]
	}

	def save = {
		def itemInstance = new Item(params)
		if (itemInstance.save(flush: true)) {
			//            flash.message = "${message(code: 'default.created.message', args: [message(code: 'item.label', default: 'Item'), itemInstance.id])}"
			//            redirect(action: "show", id: itemInstance.id)
			if("xml".equals(params.requesttype)){
				render itemInstance as XML
			}else if("json".equals(params.requesttype)){
				render itemInstance as JSON
			}else{
				render itemInstance as JSON
			}
		}
		else {
			//            render(view: "create", model: [itemInstance: itemInstance])
			if("xml".equals(params.requesttype)){
				render itemInstance.errors as XML
			}else if("json".equals(params.requesttype)){
				render itemInstance.errors as JSON
			}else{
				render itemInstance.errors as JSON
			}
		}
	}

	def show = {
		grails.converters.XML.use('deep')
		def itemInstance = Item.get(params.id)
		if (!itemInstance) {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
			//			redirect(action: "list")
			render itemInstance.errors as XML
		}
		else {
			render itemInstance as XML
			//			render(view: "edit", model: [itemInstance: itemInstance])
		}
	}

	def edit = {
		grails.converters.XML.use('deep')
		def itemInstance = Item.get(params.id)
		if (!itemInstance) {
			//            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
			//            redirect(action: "list")
			render (itemInstance.errors) as XML
		}
		else {
			render (itemInstance) as XML
		}
	}

	def update = {
		def itemInstance = Item.get(params.id)
		if (itemInstance) {
			if (params.version) {
				def version = params.version.toLong()
				if (itemInstance.version > version) {

					itemInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [
						message(code: 'item.label', default: 'Item')]
					as Object[], "Another user has updated this Item while you were editing")
					render (itemInstance.errors) as XML
					return
				}
			}
			itemInstance.properties = params
			if (!itemInstance.hasErrors() && itemInstance.save(flush: true)) {
				flash.message = "${message(code: 'default.updated.message', args: [message(code: 'item.label', default: 'Item'), itemInstance.id])}"
				render itemInstance as XML
			}
			else {
				render (itemInstance.errors) as XML
			}
		}
		else {
			//			if (!itemInstance.hasErrors() && itemInstance.save(flush: true)) {
			//				flash.message = "${message(code: 'default.updated.message', args: [message(code: 'item.label', default: 'Item'), itemInstance.id])}"
			//				render (itemInstance) as XML
			//			}
			//			else {
			render(view: "edit", model: [itemInstance: itemInstance])
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
			redirect(action: "list")
			//				render (itemInstance.errors) as XML
			//			}
		}
	}

	def delete = {
		def itemInstance = Item.get(params.id)
		if (itemInstance) {
			try {
				itemInstance.delete(flush: true)
				flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
				redirect(action: "list")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])}"
			redirect(action: "list")
		}
	}

	def getItemsForSellForItemId = {

		grails.converters.XML.use('deep')
		if(!params.itemcode) return

			def c = Item.createCriteria()
		def results = c.list {
			and{
				eq("itemcode", params.itemcode)
				eq("status","market")
				eq("type","sell")
			}
		}

		if(!results) render results.errors as XML

		render results as XML
	}
	
	def getItemsForSale = {
		
				grails.converters.XML.use('deep')
				if(!params.itemcode) return
		
					def c = Item.createCriteria()
				def results = c.list {
					and{
						or{
							eq("itemcode", params.itemcode)
							like("itemname","%"+params.itemcode+"%")
							like("itemdescription","%"+params.itemcode+"%")
						}
						
						eq("status","market")
						eq("type","sell")
					}
				}
		
				if(!results) render results.errors as XML
		
				render results as XML
			}


	def getItemsCountForItemCode = {

		grails.converters.XML.use('deep')
		if(!params.itemcode) return

			def c = Item.createCriteria()
		def results = c.get {
			projections{ count('itemcode') }
			and{
				//				try{
				//					eq("itemcode", Long.parseLong(params.itemcode))
				//				}
				//				catch(Exception e){
				eq("itemcode", params.itemcode)
				//				}
				eq("status","market")
				eq("type",params.type)
				ne("profilecode",params.userid)
			}
		}
		render results
	}

	static transactional = true


	def filelist = {
		def fileResourceInstanceList = []
		def f = new File( grailsApplication.config.images.location.toString() )
		if( f.exists() ){
			f.eachFile(){ file->
				if( !file.isDirectory() )
					fileResourceInstanceList.add( file.name )
			}
		}
		[ fileResourceInstanceList: fileResourceInstanceList ]
	}

	def deletefile = {
		def filename = params.id.replace('###', '.')
		def file = new File( grailsApplication.config.images.location.toString() + File.separatorChar +   filename )
		file.delete()
		flash.message = "file ${filename} removed"
		redirect( action:upload )
	}

	def upload = {
		def f = request.getFile('fileUpload')
		println  grailsApplication.config.images.location.toString() + File.separatorChar + f.getOriginalFilename()
		if(!f.empty) {
			flash.message = 'Your file has been uploaded'
			new File( grailsApplication.config.images.location.toString() ).mkdirs()
			f.transferTo( new File( grailsApplication.config.images.location.toString() + File.separatorChar + f.getOriginalFilename() ) )
		}
		else {
			flash.message = 'file cannot be empty'
		}
		redirect( action:filelist)
	}
}
