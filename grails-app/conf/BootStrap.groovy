import grails.util.Environment;

import com.invbox.bo.*;

class BootStrap {

	def init = { servletContext ->

		switch (Environment.current) {

			case Environment.DEVELOPMENT:
				createCategories()
				break;
			case Environment.PRODUCTION:
				createCategories()
				break;
			case Environment.TEST:
				createCategories()
				break;
		}
	}
	def destroy = {
	}
	void createCategories() {

		new Category(categorycode : "1", categoryname : "Antiques and Art", categorydescription : "Antiques and Art" ).save()
		new Category(categorycode : "2", categoryname : "Automobiles and Motorcycles", categorydescription :   "Automobiles and Motorcycles" ).save()
		new Category(categorycode : "3", categoryname : "Books", categorydescription : "Books" ).save()
		new Category(categorycode : "4", categoryname : "Businesses For Sale", categorydescription : "Businesses For Sale" ).save()
		new Category(categorycode : "5", categoryname : "Business and Industrial Equip.", categorydescription : "Business and Industrial Equip." ).save()
		new Category(categorycode : "6", categoryname : "Clothing and Accessories", categorydescription : "Clothing and Accessories" ).save()
		new Category(categorycode : "7", categoryname : "Coins Paper Money", categorydescription : "Coins Paper Money" ).save()
		new Category(categorycode : "8", categoryname : "Collectibles", categorydescription : "Collectibles" ).save()
		new Category(categorycode : "9", categoryname : "Computers and Networking", categorydescription : "Computers and Networking" ).save()
		new Category(categorycode : "10", categoryname : "Coupons", categorydescription : "Coupons" ).save()
		new Category(categorycode : "11", categoryname : "Crafts", categorydescription : "Crafts" ).save()
		new Category(categorycode : "12", categoryname : "Dolls and Dolls Houses", categorydescription : "Dolls and Dolls Houses" ).save()
		new Category(categorycode : "13", categoryname : "DVDs , Movies and TV", categorydescription : "DVDs , Movies and TV" ).save()
		new Category(categorycode : "14", categoryname : "Electronics", categorydescription : "Electronics" ).save()
		new Category(categorycode : "15", categoryname : "Gaming  Video", categorydescription : "Gaming  Video" ).save()
		new Category(categorycode : "16", categoryname : "Gift certificates", categorydescription : "Gift certificates" ).save()
		new Category(categorycode : "17", categoryname : "Health and Beauty", categorydescription : "Health and Beauty" ).save()
		new Category(categorycode : "18", categoryname : "Home and Garden", categorydescription : "Home and Garden" ).save()
		new Category(categorycode : "19", categoryname : "Jewelry and Watches", categorydescription : "Jewelry and Watches" ).save()
		new Category(categorycode : "20", categoryname : "Music  Instruments", categorydescription : "Music  Instruments" ).save()
		new Category(categorycode : "21", categoryname : "Pet Supplies", categorydescription : "Pet Supplies" ).save()
		new Category(categorycode : "22", categoryname : "Photography", categorydescription : "Photography" ).save()
		new Category(categorycode : "23", categoryname : "Pottery and Glass", categorydescription : "Pottery and Glass" ).save()
		new Category(categorycode : "24", categoryname : "Real Estate Properties", categorydescription : "Real Estate Properties" ).save()
		new Category(categorycode : "25", categoryname : "Services", categorydescription : "Services" ).save()
		new Category(categorycode : "26", categoryname : "Sports Memorabalia  Cards", categorydescription : "Sports Memorabalia  Cards" ).save()
		new Category(categorycode : "27", categoryname : "Sporting Goods", categorydescription : "Sporting Goods" ).save()
		new Category(categorycode : "28", categoryname : "Stamps", categorydescription : "Stamps" ).save()
		new Category(categorycode : "29", categoryname : "Tickets and Travel", categorydescription : "Tickets and Travel" ).save()
		new Category(categorycode : "30", categoryname : "Toys and Games", categorydescription : "Toys and Games" ).save()
		new Category(categorycode : "31", categoryname : "Wholesale Items", categorydescription : "Wholesale Items" ).save()
		new Category(categorycode : "32", categoryname : "Everything Else", categorydescription : "Everything Else" ).save()
	}
}
